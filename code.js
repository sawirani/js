function buttonClick() {
  var x1 = document.getElementById('x1').value;
  var x2 = document.getElementById('x2').value;
  var inputs = document.getElementsByName('operation');
  var operation = '';

  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].checked === true) {
      operation = inputs[i].id;
    }
  }

  if (x1 === '' || x2 === '') {
    alert('Поля x1 и x2 должны быть заполнены.')
  }

  x1 = parseInt(x1);
  x2 = parseInt(x2);

  if (Number.isNaN(x1) || Number.isNaN(x2)) {
    alert('В поля x1 и x2 должны быть введены числовые значения.')
  } else if (x1 >= x2) {
    alert('Значение поля x2 должно быть больше значения поля x1.')
  } else {

    var resultDiv = document.getElementById('result');
    resultDiv.innerHTML = '';
    var diff = x2 - x1;

    if (operation === 'sum') {
      var sum = x1;
      x1++;

      for (i = 0; i < diff; i++) {
        sum += x1++;
      }
      resultDiv.append('sum: ' + sum);
    }
    else if (operation === 'composition') {
      var comp = x1;
      x1++;

      for (i = 0; i < diff; i++) {
        comp *= x1++;
      }
      resultDiv.append('comp: ' + comp);
    }
    else if (operation === 'prime') {

      var numbers = [];
      var prime = true;

      for (i = 0; i <= diff; i++) {
        prime = true;
        for (var j = 2; j < i; j++) {
          if (x1 % j === 0) {
            prime = false;
            break;
          }
        }
        if (prime === true) {
          numbers.push(x1);
        }
        x1++;
      }

      if (numbers.length === 0) {
        resultDiv.append('Простых чисел нет.');
      } else {
        resultDiv.append('numbers: ' + numbers);
      }
    }
  }

}

function buttonClear(){
  document.getElementById('x1').value = '';
  document.getElementById('x2').value = '';
}